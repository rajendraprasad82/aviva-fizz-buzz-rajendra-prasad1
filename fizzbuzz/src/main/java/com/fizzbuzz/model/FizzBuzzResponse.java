package com.fizzbuzz.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "fizzbuzz")
@XmlAccessorType(XmlAccessType.FIELD)
public class FizzBuzzResponse {
	private List<String> outputList;

	public List<String> getOutputList() {
		return outputList;
	}

	public void setOutputList(List<String> outputList) {
		this.outputList = outputList;
	}
}
