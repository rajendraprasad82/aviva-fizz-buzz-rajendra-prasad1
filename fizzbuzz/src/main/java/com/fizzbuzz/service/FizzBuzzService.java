package com.fizzbuzz.service;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.fizzbuzz.model.FizzBuzzResponse;
import com.fizzbuzz.util.FizzBuzzUtil;

@Path("/service")
@Produces(MediaType.APPLICATION_XML)
public class FizzBuzzService {
	
	@GET
	@Path("/{number}")
	public Response generateNumbers(@PathParam("number") String number, @QueryParam("previous") int previous, @QueryParam("next") int next){
		FizzBuzzResponse response = null;
		List<String> outputList = null;
		try{
			int intNumber = Integer.parseInt(number);
			
			if(!FizzBuzzUtil.isNumberValid(intNumber)){
				return Response.status(Status.BAD_REQUEST).entity("The given number is not a postive interger").type(MediaType.TEXT_PLAIN).build();
			}
			
			if(!FizzBuzzUtil.validateNumberinRange(intNumber)){
				return Response.status(Status.BAD_REQUEST).entity("The given number is not in range between 1 - 1000").type(MediaType.TEXT_PLAIN).build();
			}
			
			outputList = FizzBuzzUtil.generateNumbers(intNumber);
			outputList = FizzBuzzUtil.formatAndFilterOutput(outputList, previous, next);
			response = new FizzBuzzResponse();
			response.setOutputList(outputList);
		}catch(NumberFormatException e){
			e.printStackTrace();
			return Response.status(Status.BAD_REQUEST).entity("The given value is not a numeric value").type(MediaType.TEXT_PLAIN).build();
		}catch(Exception e){
			e.printStackTrace();
		}
		return Response.status(Status.OK).entity(response).build();
	}
}
