package com.fizzbuzz.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class FizzBuzzUtil {
	
	private static int DAY_WEDNESDAY = 4;
	
	public static boolean isNumberValid(int number){
		if(number < 1){
			return false;
		}
		return true;
	}
	
	public static boolean validateNumberinRange(int number){
		if(number < 1 || number >1000){
			return false;
		}
		return true;
	}
	
	public static List<String> generateNumbers(int number){
		List<String> outputList = new ArrayList<String>();
		int day = getDay();
		for(int i=1;i<number;i++){
			if((i%3==0) && (i%5==0)){
				if(day==DAY_WEDNESDAY)
					outputList.add("wizz wuzz");
				else
					outputList.add("fizz buzz");
			}else if(i%3==0){
				if(day==DAY_WEDNESDAY)
					outputList.add("wizz");
				else
					outputList.add("fizz");
			}else if(i%5==0){
				if(day==DAY_WEDNESDAY)
					outputList.add("wuzz");
				else
					outputList.add("buzz");
			}else{
				outputList.add(""+i);
			}
		}
		return outputList;
	}
	
	private static int getDay(){
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.DAY_OF_WEEK);
	}
	
	public static List<String> formatAndFilterOutput(List<String> outputList, int previous,int next){
		if(previous==0 && next==0){
			previous = 0;
			next = 20;
			outputList = outputList.subList(previous, next);
		}else{
			if(next>(previous+20)){
				next = previous+20;
			}
			outputList = outputList.subList(previous, next);
		}
		return outputList;
	}
}
