package com.fizzbuzz.test;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.fizzbuzz.util.FizzBuzzUtil;

public class FizzBuzzServiceTest {
	
	@Test
	public void testNotPostiveInteger(){
		int number = -1;
		assertTrue("The given number is not a positive integer", !FizzBuzzUtil.isNumberValid(number));
	}
	
	@Test
	public void testPositiveInteger(){
		int number = 121;
		assertTrue("is a positive integer", FizzBuzzUtil.isNumberValid(number));
	}
	
	@Test
	public void testNumberNotInRange(){
		int number = 1100;
		assertTrue("is not between 1 - 1000 ", !FizzBuzzUtil.validateNumberinRange(number));
	}
	
	@Test
	public void testNumberIsInRange(){
		int number = 598;
		assertTrue("in specified range ", FizzBuzzUtil.validateNumberinRange(number));
	}
	
	@Test
	public void testGenerateNumbers() {
		int number = 6;
		/*List<String> actual = Arrays.asList("1", "2","fizz", "4", "buzz");
		System.out.println(actual);
		System.out.println(FizzBuzzUtil.generateNumbers(number));
		assertThat(actual, containsInAnyOrder(FizzBuzzUtil.generateNumbers(number)));*/
		
	}
	
}
